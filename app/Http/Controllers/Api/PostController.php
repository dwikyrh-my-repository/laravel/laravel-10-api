<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        # get all post data
        $posts = Post::query()->latest()->paginate(5);

        # return collection of posts as resource
        return new PostResource(true, 'List data posts', $posts);
    }

    public function store(Request $request)
    {
        # define validation rules
        $validator = Validator::make($request->all(), [
            'image' => ['required', 'image', 'mimes:png,jpg,jpeg,svg,gif', 'max:2048'],
            'title' => ['required', 'max:255'],
            'content' => ['required']
        ]);

        # check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # handle upload image
        $image = $request->file('image');
        $image->storeAs('public/posts/', $image->hashName());

        # action create posts
        $post = Post::query()->create([
            'image' => $image->hashName(),
            'title' => $request->title,
            'content' => $request->content,
        ]);

        # return response
        return new PostResource(true, 'Data post has been created', $post);
    }

    public function show($id)
    {
        # find post by id
        $post = Post::query()->find($id);

        # return single post as a resource
        return new PostResource(true, 'Detail data post', $post);
    }

    public function update(Request $request, $id)
    {
        # define validation rules
        $validator = Validator::make($request->all(), [
            'image' => ['nullable', 'image', 'mimes:png,jpg,jpeg,svg,gif', 'max:2048'],
            'title' => ['required', 'max:255'],
            'content' => ['required']
        ]);

        # check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # find post by id
        $post = Post::find($id);

        # check if image not empty
        if ($request->hasFile('image'))
        {
            # upload image
            $image = $request->file('image');
            $image->storeAs('public/posts', $image->hashName());

            # delete old image
            Storage::delete('public/posts/'.basename($post->image));

            # update post with new image
            $post->update([
                'image' => $image->hashName(),
                'title' => $request->title,
                'content' => $request->content,
            ]);
        } else {
            # update post without image
            $post->update([
                'title' => $request->title,
                'content' => $request->content,
            ]);
        }

        # return response
        return new PostResource(true, 'Post data has been updated', $post);
    }

    public function destroy($id)
    {
        # find post by id
        $post = Post::find($id);
        
        # delete image
        Storage::delete('public/posts/'.basename($post->image));
        
        # delete post
        $post->delete();

        # return response
        return new PostResource(true, 'Post data has been deleted', null);
    }
}
